import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Ejercicio2 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        Set <String> empleados = new HashSet<>();
        int opc = 0;

        do{
            System.out.print("Ingrese el nombre del empleado (solo el primer nombre):");
            empleados.add(entrada.nextLine());
            System.out.println("Desea agregar otro nombre a la lista?");
            System.out.println("1.Si");
            System.out.println("2.No");
            System.out.print("Opcion: ");
            opc = entrada.nextInt();
            System.out.println();
            entrada.nextLine();
        }while(opc != 2);

        entrada.close();
        imprimirLista(empleados);
    }

    public static void imprimirLista(Set list) {
        System.out.println("\nElementos de la lista: ");
        for(Object name : list){
            System.out.println(name.toString());
        }
    }
}
