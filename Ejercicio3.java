import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ejercicio3 {
    public static void main(String[] args) {
        int opc = 0;
        Scanner scan = new Scanner(System.in);
        List empleados = new ArrayList<>();


        do{

            System.out.print("Ingrese el nombre del empleado: ");
            empleados.add(scan.nextLine());

            System.out.println("\nDesea ingresar otro nombre a la lista?");
            System.out.println("1.SI");
            System.out.println("2.NO");
            System.out.print("Opcion: ");
            opc = scan.nextInt();

            scan.nextLine();
        }while(opc != 2);

        System.out.println("\tLISTA DE ESPERA");
        imprimirLista(empleados);

        scan.close();
}

public static void imprimirLista(List list) {
    for(Object name : list){
        System.out.println("\t" + name.toString());
    }
}

}
