
import java.util.Arrays;
import java.util.Comparator;


public class ArrayObject {
    
    //un arreglo de 10 objetos (Persona) desordenados
    static Persona[] personas = {
        new Persona("Jose", 22),
        new Persona("Eduardo", 31),
        new Persona("Mike", 60),
        new Persona("Alonso", 38),
        new Persona("Lenny", 40),
        new Persona("Travis", 24),
        new Persona("Zelmar", 76),
        new Persona("Lucy", 47),
        new Persona("Arnold", 35),
        new Persona("Isabella", 19)
    };
 
    /**
     * Punto de entrada del programa.
     *
     * @param args argumentos de la línea de comandos.
     */
    public static void main(String[] args) {
        //imprimimos por pantalla el arreglo de personas
        System.out.println("Arreglo sin orden:");
        for (Persona p : personas) {
            System.out.println(p);
        }
 
        System.out.println();
 
        //ordenamos naturalmente el arreglo de personas y lo imprimimos por pantalla
        System.out.println("En orden natural:");
        Arrays.sort(personas);
        for (Persona p : personas) {
            System.out.println(p);
        }
 
        System.out.println();
 
        //ordenamos con nuestro comparador el arreglo de personas y lo imprimimos por pantalla
        System.out.println("En orden segun el comparador:");
        Arrays.sort(personas, new OrderByAge());
        for (Persona p : personas) {
            System.out.println(p);
        }
    }
}
 
/**
 * Esta clase representa una persona almacenando su nombre y su edad.
 * Implementa Comparable para establecer el orden natural en caso de
 * ser comparado con int compareTo(Object o).
 */
class Persona implements Comparable {
 
    private final String nombre;
    private final int edad;
 
    /**
     * Constructor de la clase.
     *
     * @param nombre el nombre de la persona.
     * @param edad la edad de la persona.
     */
    public Persona(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }
 
    public String getNombre() {
        return nombre;
    }
 
    public int getEdad() {
        return edad;
    }
 
    @Override
    public String toString() {
        return nombre + " - " + edad;
    }
 
    /**
     * En este método comparamos el dato que preferimos de nuestro objeto con el correspondiente
     * de otro objeto.
     *
     * @param o el objeto con el cual se hará una comparación.
     * @return según la implementación en la clase String se realiza una comparación lexicográfica
     * de los dos nombres (strings): 0 si son iguales, -1 si este es menor o 1 si este es mayor.
     */
    @Override
    public int compareTo(Object o) {
        //hacemos un casting del objeto pasado como argumento
        Persona p = (Persona) o;
 
        //comparamos el nombre de este objeto con el nombre del otro objeto
        return this.nombre.compareTo(p.getNombre());
    }
}
 
/**
 * Esta clase es nuestro comparador, implementa la interface Comparator.
 */
class OrderByAge implements Comparator {
 
    /**
     * En este método comparamos la edad de dos objetos Persona.
     *
     * @param o1 primer persona.
     * @param o2 segunda persona.
     * @return según la comparación de edades: 0 si son iguales, 1 si el primero es mayor que el
     * segundo o -1 si el primero es menor que el segundo.
     */

    @Override
    public int compare(Object o1, Object o2) {
        Persona p1 = (Persona)o1;
        Persona p2 = (Persona)o2;
        if (p2.getEdad() == p1.getEdad()) {          //si la edad de la primer persona es igual a la edad de la segunda retornamos 0
            return 0;
        } else if (p1.getEdad() > p2.getEdad()) {    //si la edad de la primer persona es mayor la edad de la segunda retornamos 1
            return 1;
        } else {                           //si la edad de la primer persona es menor la edad de la segunda retornamos -1
            return -1;
        }
    }
}
